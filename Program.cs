﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
namespace CS_TESTING
{
	public static class Limits
	{
		public static uint MaxMessageLength = 2000;
		public static uint MaxBitmapToASCIIWidth = 150;
	}
	class Program
	{
		public struct ASCIIImage
		{
			public uint width;
			public uint height;
			public char[,] ImageContents;
		}

		public static ASCIIImage? ConvertBitmapToASCII(Bitmap img)
		{
			if (img == null)
				return null;

			if (img.PixelFormat == PixelFormat.Undefined)
			{
				MessageBox.Show("The specified image file is broken, you will need to import and export this file from an image editing software. (PixelFormat == UNDEFINED)");
				return null;
			}
			else if (img.PixelFormat == PixelFormat.Indexed || img.PixelFormat == PixelFormat.Gdi)
			{
				MessageBox.Show("The specified image file is either grayscale or broken. Please add RGB channels or fix this image, as it's not possible to parse indexed images (PixelFormat == INDEXED");
				return null;
			}

			ASCIIImage returnedImg = new ASCIIImage();

			MessageBox.Show(img.PixelFormat.ToString());

			if(img.Width > 100 && img.Width > 0)
			{
				float aspectRatio = img.Width / img.Height;
				int width = 100;
				float height = (img.Height * (100.0f / img.Width));
				Bitmap f = new Bitmap(img, width, (int)height);
				img.Dispose();
				img = f;
			}

			Bitmap gd = new Bitmap(img, (int)(img.Width * 2), (int)(img.Height));
			img.Dispose();
			img = gd;

			returnedImg.ImageContents = new char[img.Width, img.Height];
			returnedImg.width = (uint)img.Width;
			returnedImg.height = (uint)img.Height;

			//Convert to grayscale
			for (int x = 0; x < img.Width; x++)
			{
				for (int y = 0; y < img.Height; y++)
				{
					Color c = img.GetPixel(x, y);
					byte avg = (byte)((c.R + c.G + c.B) / 3);
					c = Color.FromArgb(avg, avg, avg);
					img.SetPixel(x, y, c);
				}
			}

			for (int y = 0; y < img.Height; y++)
			{
				for (int x = 0; x < img.Width; x++)
				{
					//alreadt grayscale, so we can assume r == g == b
					byte clr = img.GetPixel(x, y).R;

					if (clr <= 20)
						returnedImg.ImageContents[x, y] = ' ';
					else if (clr <= 40)
						returnedImg.ImageContents[x, y] = '`';
					else if (clr <= 60)
						returnedImg.ImageContents[x, y] = '.';
					else if (clr <= 80)
						returnedImg.ImageContents[x, y] = '*';
					else if (clr <= 100)
						returnedImg.ImageContents[x, y] = '!';
					else if (clr <= 120)
						returnedImg.ImageContents[x, y] = 'i';
					else if (clr <= 140)
						returnedImg.ImageContents[x, y] = 'j';
					else if (clr <= 160)
						returnedImg.ImageContents[x, y] = 'l';
					else if (clr <= 180)
						returnedImg.ImageContents[x, y] = '0';
					else if (clr <= 200)
						returnedImg.ImageContents[x, y] = 'F';
					else if (clr <= 220)
						returnedImg.ImageContents[x, y] = 'R';
					else if (clr <= 240)
						returnedImg.ImageContents[x, y] = 'B';
					else if (clr <= 255)
						returnedImg.ImageContents[x, y] = '#';
					else
						returnedImg.ImageContents[x, y] = '@';
				}
			}

			return returnedImg;
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Stupid image to ASCII converter thing. I wrote this thing at 4AM and it worked first try. I am happy. Now kill me.");
			Console.WriteLine("Type convert -f <file path> -o <out file path> to convert a file. Leave -o empty for an out.txt in the same directory.");
			Console.Write('\n');
			while(true)
			{
				string[] input = Console.ReadLine().Split(' ');
				if(input.Length >= 3)
				{
					string path = input[2];
					string outpath = "out.txt";
					if(input.Length >= 5)
					{
						outpath = input[4];
					}

					try
					{
						Bitmap bmp = (Bitmap)Bitmap.FromFile(path);
						ASCIIImage img = ConvertBitmapToASCII(bmp).GetValueOrDefault();

						//This wall of text will tell the user if there's a bad image
						if(bmp.PixelFormat == PixelFormat.Alpha || bmp.PixelFormat == PixelFormat.Canonical || bmp.PixelFormat == PixelFormat.DontCare ||
							bmp.PixelFormat == PixelFormat.Extended || bmp.PixelFormat == PixelFormat.Gdi || bmp.PixelFormat == PixelFormat.Indexed ||
							bmp.PixelFormat == PixelFormat.Max || bmp.PixelFormat == PixelFormat.PAlpha || bmp.PixelFormat == PixelFormat.Undefined)
						{
							MessageBox.Show("The format " + bmp.PixelFormat + " is unrecognized. Please ensure your image uses a non-grayscale format, and try exporting your image again from an image editor");
							throw new BadImageFormatException("The image format is invalid!");
						}

						StreamWriter f = new StreamWriter(outpath);
						Console.WriteLine("Writing to file...");

						for (uint y = 0; y < img.height; y++)
						{
							for (uint x = 0; x < img.width; x++)
							{
								f.Write(img.ImageContents[x, y]);
								if (x == img.width - 1)
									f.Write('\n');
							}
						}

						f.Flush();
						Console.WriteLine("Finished! FINITO!\n");
						Console.ReadKey();
					}
					catch(Exception e)
					{
						//MessageBox.Show(e.StackTrace);
					}
				}
				else
				{
					Console.WriteLine("Incorrect syntax. -_- Correct syntax looks a bit like this: convert -f <file path> -o <out file path> Ex: convert -f image.jpg -o out.txt\n");
				}
			}

		}
	}
}
