# BitmapToASCII

This program is used to convert a bitmap into an array of characters that looks like the image.
Currently it supports converting a color image to raw ASCII.

When converting, the image is scaled, converted to grayscale, read pixel by pixel then written to file.

An updated version of this program is included with DiscordBotCS. The new version even supports colored HTML!